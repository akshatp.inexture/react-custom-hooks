import React, { useState } from "react";
import useFetch from "../Hooks/useFetch";
const Task1 = () => {
  const [url, seturl] = useState("");
  const [option, setOption] = useState("");
  const { Fetch, data, Reset, error, checktype } = useFetch(url, option);
  const getdata = () => {
    if (url == "") {
      window.alert("Please Enter URL");
    } else if (option == "") {
      window.alert(" Please select option");
    } else {
      Fetch();
    }
  };
  console.log(data);
  return (
    <div>
      <hr />
      <h3>Get Data</h3>
      <input
        type="text"
        value={url}
        onChange={(e) => seturl(e.target.value)}
        placeholder="Enter URL"
      />
      <button className="btn btn-outline-primary" onClick={getdata}>
        Get Data
      </button>
      <button className="btn btn-outline-primary" onClick={Reset}>
        Reset
      </button>
      <br />
      <div class="form-check">
        <input
          type="radio"
          name="storage"
          className="form-check-input"
          value="posts"
          onChange={(e) => setOption(e.target.value)}
        />
        <label className="form-check-label">Post</label>
      </div>
      <div class="form-check">
        <input
          type="radio"
          name="storage"
          className="form-check-input"
          value="comments"
          onChange={(e) => setOption(e.target.value)}
        />
        <label className="form-check-label">Comments</label>
      </div>
      <div class="form-check">
        <input
          type="radio"
          name="storage"
          className="form-check-input"
          value="users"
          onChange={(e) => setOption(e.target.value)}
        />
        <label className="form-check-label">User</label>
      </div>
      <div class="form-check">
        <input
          type="radio"
          name="storage"
          className="form-check-input"
          value="todos"
          onChange={(e) => setOption(e.target.value)}
        />
        <label className="form-check-label">Todos</label>
      </div>
      <hr />
      <>
        {data &&
          data.map((item) => (
            <>
              {checktype === "users" && (
                <p key={item.id}>
                  <b>Name: </b>
                  {item.name}
                  <br />
                  <b>Email: </b>
                  {item.email}
                </p>
              )}

              {checktype === "comments" && (
                <p key={item.id}>
                  <b>Name:</b> {item.name}
                  <br />
                  <b>Email:</b> {item.email}
                  <br />
                  <b>Comment:</b> {item.body}
                </p>
              )}
              {checktype === "posts" && (
                <p key={item.id}>
                  <b>Post:</b> {item.title}
                  <br />
                  <b>Content:</b> {item.body}
                </p>
              )}
              {checktype === "todos" && (
                <p key={item.id}>
                  <b>Task: </b>
                  {item.title}
                  <br />
                  <b>Completed:</b> {item.completed?.toString()}
                </p>
              )}
            </>
          ))}
      </>
      <p>{error}</p>
    </div>
  );
};

export default Task1;
