import React from "react";
import useLocation from "../Hooks/useLocation";

const Task3 = () => {
  const { latitude, longitude, getlocation } = useLocation();
  return (
    <div>
      <h3>User location </h3>
      <p>
        Latitude: {latitude} <br />
        Longitude: {longitude}
      </p>
      <button className="btn btn-outline-primary" onClick={getlocation}>
        Get Location
      </button>
      <hr />
    </div>
  );
};

export default Task3;
