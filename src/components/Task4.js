import React, { useState } from "react";
import useValidation from "../Hooks/useValidation";

const Task4 = () => {
  const [email, setEmail] = useState("");
  const [validate, error, isvalid] = useValidation();
  const handleClick = () => {
    validate(email);
  };

  return (
    <div>
      <h3>Email Validation </h3>
      Email:{" "}
      <input
        type="text"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        placeholder="Enter your email..."
      />
      <button className="btn btn-outline-primary" onClick={handleClick}>
        Validate
      </button>
      {isvalid && <p className="alert alert-success">Email is valid</p>}
      {error && <p className="text-danger mb-2">Email is Invalid!</p>}
      <hr />
    </div>
  );
};

export default Task4;
