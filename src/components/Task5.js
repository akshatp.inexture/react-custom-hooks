import React, { useState } from "react";
import useSave from "../Hooks/useSave";
const Task5 = () => {
  const [key, setKey] = useState("");
  const [value, setValue] = useState("");
  const [storage, setStorage] = useState("");

  const [isSaved, Local, Session] = useSave(key, value, storage);

  const handleClick = () => {
    if (key !== "" && value !== "") {
      if (storage === "local") {
        Local();
      } else if (storage === "session") {
        Session();
      } else {
        window.alert("Please Choose a storage type!");
      }
    } else {
      window.alert("Key and Value are required!");
    }
  };
  return (
    <div>
      <h3>Store Data to Storage</h3>
      {isSaved && <p className="alert alert-success">Saved</p>}

      <div className="text-start">
        <p>
          <label>Key:</label>
          <input
            type="text"
            value={key}
            onChange={(e) => setKey(e.target.value)}
            placeholder="Enter the key"
          />
        </p>
        <p>
          <label> Value:</label>
          <input
            type="text"
            value={value}
            onChange={(e) => setValue(e.target.value)}
            placeholder="Enter the Value"
          />
        </p>
        <p>
          Storage:
          <br />
          <label>Local</label>
          <input
            type="radio"
            name="storage"
            value="local"
            onChange={(e) => setStorage(e.target.value)}
          />
          <label>Session</label>
          <input
            type="radio"
            name="storage"
            value="session"
            onChange={(e) => setStorage(e.target.value)}
          />
        </p>
        <p>
          <button className="btn btn-outline-primary" onClick={handleClick}>
            Store
          </button>
        </p>
      </div>
      <hr />
    </div>
  );
};

export default Task5;
