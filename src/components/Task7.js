import React, { useState } from "react";
import useCopy from "../Hooks/useCopy";

const Task7 = () => {
  const [insert, setinsert] = useState("");
  const { value, copyfunction } = useCopy("");
  const arrayPush = () => {
    copyfunction(insert);
  };
  return (
    <div>
      <h3>Copy to clipboard</h3>
      <input
        type="text"
        value={insert}
        onChange={(e) => setinsert(e.target.value)}
      />
      <button className="btn btn-primary" onClick={() => arrayPush()}>
        Copy text
      </button>
      <p className="text-success">Copied Text: {value}</p>
    </div>
  );
};

export default Task7;
