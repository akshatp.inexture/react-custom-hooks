import React from "react";
import useWindow from "../Hooks/useWindow";

const Task2 = () => {
  const { width, height, resolution, getWindowdata } = useWindow();
  return (
    <div>
      <h3>Window size</h3>
      <>
        <p>Width: {width}</p>
        <p>height: {height}</p>
        <p>Resolution: {resolution}</p>
      </>
      <button className="btn btn-outline-primary" onClick={getWindowdata}>
        Get Screen
      </button>
      <hr />
    </div>
  );
};

export default Task2;
