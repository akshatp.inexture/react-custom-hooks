import React, { useState } from "react";
import useArray from "../Hooks/useArray";

const Task6 = () => {
  const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  const [answer, reverse, push, pop] = useArray(array);
  const [value, setvalue] = useState("");
  //   console.log(answer);

  const arrayReverse = () => {
    reverse();
  };
  const arrayPush = (val) => {
    if (value !== "") push(val);
  };
  const arrayPop = () => {
    pop();
  };

  return (
    <div>
      <h3>Array Methods</h3>

      <p>Given Array: {array.join(",")}</p>

      <p>
        <label> Value:</label>
        <input
          type="text"
          placeholder="Enter the Value"
          value={value}
          onChange={(e) => setvalue(e.target.value)}
        />
      </p>
      <button className="btn btn-primary" onClick={() => arrayPush(value)}>
        Push
      </button>
      <button className="btn btn-primary" onClick={arrayPop}>
        Pop
      </button>

      <button className="btn btn-primary" onClick={arrayReverse}>
        Reverse
      </button>

      <p>
        Result:
        {answer.toString("")}
      </p>
      <hr />
    </div>
  );
};

export default Task6;
