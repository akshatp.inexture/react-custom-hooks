import "./App.css";
// import React, { useState, useMemo } from "react";
import Task2 from "./components/Task2";
import Task3 from "./components/Task3";
import Task6 from "./components/Task6";
import Task7 from "./components/Task7";
import Task1 from "./components/Task1";
import Task4 from "./components/Task4";
import Task5 from "./components/Task5";
function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="">
          <Task2 />
        </div>
      </div>
      <div className="row">
        <div className="">
          <Task3 />
        </div>
      </div>
      <div className="row">
        <div className="">
          <Task4 />
        </div>
      </div>
      <div className="row">
        <div className="">
          <Task5 />
        </div>
      </div>
      <div className="row">
        <div className="">
          <Task6 />
        </div>
      </div>
      <div className="row">
        <div className="">
          <Task7 />
        </div>
      </div>
      <div className="row">
        <div className="">
          <Task1 />
        </div>
      </div>
    </div>
  );
}

export default App;
