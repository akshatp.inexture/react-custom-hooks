import { useState } from "react";

import React from "react";

const useLocation = () => {
  const [latitude, setlatitude] = useState("");
  const [longitude, setlongitude] = useState("");
  const getlocation = () => {
    navigator.geolocation.watchPosition(function (position) {
      let latt = position.coords.latitude;
      let longg = position.coords.longitude;
      setlatitude(latt);
      setlongitude(longg);
    });
  };
  return { latitude, longitude, getlocation };
};

export default useLocation;
