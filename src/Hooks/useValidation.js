import { useState } from "react";

const useValidation = () => {
  const [error, setError] = useState(false);
  const [isvalid, setisvalid] = useState(false);
  const validate = (email) => {
    console.log("hey");
    let valid = email
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
    setError(!valid);
    if (valid) {
      setisvalid(true);
      setTimeout(() => {
        setisvalid(false);
      }, 5000);
    }
  };
  return [validate, error, isvalid];
};

export default useValidation;
