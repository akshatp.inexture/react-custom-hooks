import { useState } from "react";

import React from "react";

const useWindow = () => {
  const [width, setwidth] = useState("");
  const [height, setheight] = useState("");
  const [resolution, setresolution] = useState("");

  const getWindowdata = () => {
    setwidth(window.screen.width);
    setheight(window.screen.height);
    setresolution(`${window.screen.width}X${window.screen.height}`);
  };
  return { width, height, resolution, getWindowdata };
};

export default useWindow;
