import { useState } from "react";

const useFetch = (url, option) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [checktype, setChecktype] = useState(null);

  const Fetch = () => {
    //Runs only on the first render
    let ap = async () => {
      try {
        setChecktype(option);
        if (url[url.length - 1] !== "/") {
          url = `${url}/`;
        }
        let res = await fetch(`${url}${option}`);
        let result = await res.json();
        setData(result.slice(0, 10));
        setError("");
      } catch (err) {
        setError("Use this link:  https://jsonplaceholder.typicode.com");
      }
    };
    ap();
  };

  const Reset = () => {
    setError("");
    setData([]);
  };
  return { Fetch, data, Reset, error, checktype };
};

export default useFetch;
