import { useState } from "react";

const useArray = (array) => {
  const [answer, setanswer] = useState(array);
  const reverse = () => {
    let arr = answer;
    arr = arr.reverse();
    setanswer([...arr]);
    console.log(answer);
  };

  const push = (value) => {
    setanswer([...answer, value]);
  };

  const pop = () => {
    let arr = answer;
    arr = arr.filter((value, i) => i !== arr.length - 1);
    setanswer([...arr]);
  };

  return [answer, reverse, push, pop];
};

export default useArray;
