import { useState } from "react";

const useSave = (key, value) => {
  const [isSaved, setIsSaved] = useState(false);
  const Local = () => {
    localStorage.setItem(key, value);
    setIsSaved(true);
    setInterval(() => {
      setIsSaved(false);
    }, 10000);
  };

  const Session = () => {
    sessionStorage.setItem(key, value);
    setIsSaved(true);
    setInterval(() => {
      setIsSaved(false);
    }, 10000);
  };
  return [isSaved, Local, Session];
};

export default useSave;
