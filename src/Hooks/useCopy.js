import React, { useState } from "react";

const useCopy = () => {
  const [value, setvalue] = useState("");
  const copyfunction = (insert) => {
    navigator.clipboard.writeText(insert);
    setvalue(insert);
    // console.log(insert);
  };
  return { value, copyfunction };
};

export default useCopy;
